package com.hpe.china.sd.integration.core.api.transport;

/**
 *
 * Supported Http Client Methods
 *
 */
public enum HttpMethod {
    HEAD, GET, POST, PUT, PATCH, DELETE, OPTIONS, TRACE
}