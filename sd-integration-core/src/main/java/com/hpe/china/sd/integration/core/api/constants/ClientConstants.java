package com.hpe.china.sd.integration.core.api.constants;

public class ClientConstants {


    public static final String HEADER_X_AUTH_TOKEN = "X-Auth-Token";
    public static final String HEADER_USER_AGENT = "User-Agent";
    public static final String HEADER_SD4J_AUTH = "SD4J-Auth-Command";
    public static final String HEADER_AUTHORIZATION = "Authorization";
    public static final String USER_AGENT = "Java Client";

    public static final String HEADER_CONTENT_TYPE = "Content-Type";

    public static final String CONTENT_TYPE_JSON = "application/json";
    public static final String CONTENT_TYPE_STREAM = "application/stream";
    public static final String CONTENT_TYPE_DIRECTORY = "application/directory";
    public static final String CONTENT_TYPE_OCTECT_STREAM = "application/octet-stream";
    public static final String CONTENT_TYPE_TEXT = "text/plain";
    public static final String CONTENT_TYPE_TEXT_HTML = "text/html";
    public static final String CONTENT_TYPE_ARTIFACT_PATCH = "application/json-patch+json";


    // Paths
    public static final String URI_SEP = "/";
}
