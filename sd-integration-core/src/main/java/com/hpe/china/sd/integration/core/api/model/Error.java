package com.hpe.china.sd.integration.core.api.model;

public interface Error {

     String code();
     String description();
}
