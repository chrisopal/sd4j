package com.hpe.china.sd.integration.core.api.transport;


/**
 * Decorator interface to designate object wrapper type results and to insure that an Empty Object is returned in 404/NotFound
 * responses
 *
 */
public interface ObjectType {

}
