package com.hpe.china.sd.integration.core.api.common;


import com.hpe.china.sd.integration.core.api.model.ModelEntity;

/**
 * Possible Auth Types
 *
 */
public interface Auth extends ModelEntity {

    public enum Type { CREDENTIALS, TOKEN, RAX_APIKEY, TOKENLESS, OAUTH2 }

}
