package com.hpe.china.sd.integration.core.api.model;

import java.io.Serializable;


/**
 *
 * Decorates a Model class which is used for rest service operations
 *
 */
public interface ModelEntity extends Serializable {


}
