package com.hpe.china.sd.integration.core.api.transport;


/**
 * Decorator interface to designate List Type results and to insure that an EmptyList is returned in 404/NotFound
 * responses
 *
 */
public interface ListType {

}
