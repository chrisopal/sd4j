package com.hpe.china.sd.integration.connectors.okhttp;

import com.hpe.china.sd.integration.client.service.client.SDAuthenticator;
import com.hpe.china.sd.integration.client.service.client.SDRestClientSession;
import com.hpe.china.sd.integration.core.api.constants.ClientConstants;
import com.hpe.china.sd.integration.core.api.exceptions.ConnectionException;
import com.hpe.china.sd.integration.core.api.transport.HttpExecutorService;
import com.hpe.china.sd.integration.core.api.transport.HttpRequest;
import com.hpe.china.sd.integration.core.api.transport.HttpResponse;
import okhttp3.Response;


/**
 * HttpExecutor is the default implementation for HttpExecutorService which is responsible for interfacing with OKHttp and mapping common status codes, requests and responses
 * back to the common API
 */
public class HttpExecutorServiceImpl implements HttpExecutorService {

    private static final String NAME = "OKHttp Connector";

    /**
     * {@inheritDoc}
     */
    @Override
    public <R> HttpResponse execute(HttpRequest<R> request) {
        try {
            return invoke(request);
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Invokes the given request
     *
     * @param <R>     the return type
     * @param request the request to invoke
     * @return the response
     * @throws Exception the exception
     */
    private <R> HttpResponse invoke(HttpRequest<R> request) throws Exception {

        HttpCommand<R> command = HttpCommand.create(request);

        try {
            return invokeRequest(command);
        } catch (Exception pe) {
            throw new ConnectionException(pe.getMessage(), 0, pe);
        }
    }

    private <R> HttpResponse invokeRequest(HttpCommand<R> command) throws Exception {
        Response response = command.execute();
        //in case of Auth failed due to token expired or invalid.
        if (command.getRetries() == 0 && response.code() == 401 && !command.getRequest().getHeaders().containsKey(ClientConstants.HEADER_SD4J_AUTH)) {
            SDAuthenticator.reAuthenticate();
            command.getRequest().getHeaders().put(ClientConstants.HEADER_X_AUTH_TOKEN, SDRestClientSession.getCurrent().getTokenId());
            return invokeRequest(command.incrementRetriesAndReturn());
        }
        return HttpResponseImpl.wrap(response);
    }

    @Override
    public String getExecutorDisplayName() {
        return NAME;
    }

}
