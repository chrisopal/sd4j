SD4J
===========
SD4J is a fluent Java client for **Service Director**.  

Configure Lombok 
-----
* Download lombok version 1.18.2 or later and configure it for [Eclipse](https://projectlombok.org/setup/eclipse)
* In Intellij IDEA plugin search for lombok and install.

Maven
-----

**Using SD4J with the OkHttp Connector with 1.0.0-Snapshot**

```xml
<dependency>
    <groupId>com.hpe.china.sd</groupId>
    <artifactId>sd-integration-client</artifactId>
    <version>${project.version}</version>
</dependency>
<dependency>
    <groupId>com.hpe.china.sd</groupId>
    <artifactId>okhttp</artifactId>
    <version>${project.version}</version>
</dependency>
```

Quick Usage Guide
-----------------

Below are some examples of the API usage.

#### New Client: 
```Java
//Client Example

private static final String USERNAME = "admin";

private static final String PASSWORD = "admin";

private static final String TENANT = "CMZJNS";

private static final String ENDPOINT = "http://192.168.56.101:8081/v2";

private static SDRestClient sdRestClient = SDRestClientFactory.newClient(USERNAME, PASSWORD, TENANT, ENDPOINT);
    
```
#### Get Instance Service from Client and Make a call
```java
//Build Request
private CreateServiceRequest createServiceRequest() {

        MetaData metaData = new MetaData();
        String a = "How old are you";
        String b = "5";
        metaData.put("a", a);
        metaData.put("b", b);
        return CreateServiceRequest.builder()
                .service(
                        ServiceRequest.builder()
                                .action("create").serviceType("Adapter::SimpleService")
                                .serviceKey("ss01-001")
                                .metadata(metaData).build())
                .transactionId(UUID.randomUUID().toString())
                .build();
}
      
//Init Api Call
CreateServiceResponse createServiceResponse = client().serviceInstanceManagement().createService(TENANT, false, createServiceRequest());
LOGGER.debug("Get result: {}", createServiceResponse);
```
