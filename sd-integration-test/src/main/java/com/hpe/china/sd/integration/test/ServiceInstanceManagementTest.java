package com.hpe.china.sd.integration.test;

import com.hpe.china.sd.integration.client.model.common.MetaData;
import com.hpe.china.sd.integration.client.model.instance.CreateServiceRequest;
import com.hpe.china.sd.integration.client.model.instance.CreateServiceRequestWrapper;
import com.hpe.china.sd.integration.client.model.instance.CreateServiceResponse;
import com.hpe.china.sd.integration.client.model.instance.ServiceRequest;


import com.hpe.china.sd.integration.core.api.transport.ObjectMapperSingleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import java.util.UUID;

public class ServiceInstanceManagementTest extends AbstractTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceInstanceManagementTest.class);

    private static final String TENANT = "CMZJNS";

    //private static final String SERVICE_KEY = TENANT + "&#47;&#47;" + "ToBeFailed_1540784925337";

    @Test
    void testCreateService() throws Exception {


        String request = ObjectMapperSingleton.getContext(CreateServiceRequestWrapper.class).writer().writeValueAsString(CreateServiceRequestWrapper.builder().service(createServiceRequest()).build());

        LOGGER.debug("Request String: {}", request);
        CreateServiceResponse createServiceResponse = client().serviceInstanceManagement().createService(TENANT, false, createServiceRequest());

        LOGGER.debug("Get result: {}", createServiceResponse);

    }

    private CreateServiceRequest createServiceRequest() {

        MetaData metaData = new MetaData();
        String a = "How old are you";
        String b = "5";
        metaData.put("a", a);
        metaData.put("b", b);
        return CreateServiceRequest.builder()
                .service(
                        ServiceRequest.builder()
                                .action("create").serviceType("Adapter::SimpleService")
                                .serviceKey("ss01-001")
                                .metadata(metaData).build())
                .transactionId(UUID.randomUUID().toString())
                .build();

    }
}
