package com.hpe.china.sd.integration.test;

import com.hpe.china.sd.integration.client.api.SDRestClient;
import com.hpe.china.sd.integration.client.service.client.SDRestClientFactory;
import com.hpe.china.sd.integration.core.api.transport.HttpLoggingFilter;

public abstract class AbstractTest {

    static {
        HttpLoggingFilter.toggleLogging(true);
        System.setProperty("log4j.configurationFile","./log4j.xml");
    }

    private static final String USERNAME = "admin";

    private static final String PASSWORD = "admin";

    private static final String TENANT = "CMZJNS";

    private static final String ENDPOINT = "http://192.168.56.101:8081/v2";


    private static SDRestClient sdRestClient = SDRestClientFactory.newClient(USERNAME, PASSWORD, TENANT, ENDPOINT);

    public static SDRestClient client() { return sdRestClient; }
}
