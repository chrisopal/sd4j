package com.hpe.china.sd.integration.client.model.common;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum RollbackState {

    NORMAL,

    CANCELED,

    ROLLBACK,

    UNRECOGNIZED;


    @JsonCreator
    public static RollbackState value(String v) {
        if (v == null) return UNRECOGNIZED;
        try {
            return valueOf(v.toUpperCase());
        } catch (IllegalArgumentException e) {
            return UNRECOGNIZED;
        }
    }

    @JsonValue
    public String value() {
        return name().toUpperCase();
    }
}
