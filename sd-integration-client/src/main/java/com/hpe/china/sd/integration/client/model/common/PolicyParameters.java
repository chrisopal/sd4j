package com.hpe.china.sd.integration.client.model.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.Singular;

import java.util.List;

@Data
@Builder
public class PolicyParameters {

    @Singular
    @JsonProperty("parameter")
    private List<PolicyParameter> parameters;
}
