package com.hpe.china.sd.integration.client.service.domain;

import com.hpe.china.sd.integration.client.api.domain.ServiceInstanceManagement;
import com.hpe.china.sd.integration.client.constants.PathConstants;
import com.hpe.china.sd.integration.client.model.instance.CreateServiceRequest;
import com.hpe.china.sd.integration.client.model.instance.CreateServiceRequestWrapper;
import com.hpe.china.sd.integration.client.model.instance.CreateServiceResponse;
import com.hpe.china.sd.integration.core.api.service.BaseRestClientService;
import org.apache.commons.lang3.StringUtils;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import static com.hpe.china.sd.integration.client.model.instance.CreateServiceResponse.CreateServiceResponseWrapper;

public class ServiceInstanceManagementImpl extends BaseRestClientService implements ServiceInstanceManagement {

    @Override
    public CreateServiceResponse createService(String tenantId, boolean strictActions, CreateServiceRequest createServiceRequest) {
        checkArgument(!StringUtils.isEmpty(tenantId));
        checkNotNull(createServiceRequest);

        return post(CreateServiceResponseWrapper.class, uri(PathConstants.CREATE_SERVICE_PATH, tenantId))
                .entity(CreateServiceRequestWrapper.builder().service(createServiceRequest).build())
                .param("strictactions", strictActions)
                .execute().get(CreateServiceResponse.class);

    }
}
