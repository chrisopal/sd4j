package com.hpe.china.sd.integration.client.api.domain;

import com.hpe.china.sd.integration.client.model.instance.CreateServiceRequest;
import com.hpe.china.sd.integration.client.model.instance.CreateServiceResponse;

public interface ServiceInstanceManagement {

    /**
     * Create Service
     *
     * @param tenantId tenant id where service belongs
     * @param strictActions If set to true, this method will fail if the service instance already exists.
     * @param createServiceRequest create service request
     * @return create service response
     */
    public CreateServiceResponse createService(String tenantId, boolean strictActions, CreateServiceRequest createServiceRequest);


}
