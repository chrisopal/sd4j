package com.hpe.china.sd.integration.client.service.client;


import com.hpe.china.sd.integration.client.api.ISDRestClientBuilder;
import com.hpe.china.sd.integration.client.api.SDRestClient;
import com.hpe.china.sd.integration.client.model.idenity.Credentials;
import com.hpe.china.sd.integration.core.api.exceptions.AuthenticationException;
import com.hpe.china.sd.integration.core.api.service.BaseRestClientBuilder;

/**
 * Builder to create a client
 *
 */
public class SDRestClientBuilder extends BaseRestClientBuilder<SDRestClient, ISDRestClientBuilder> implements ISDRestClientBuilder {

    private String tenantName;


    @Override
    public ISDRestClientBuilder tenantName(String tenantName) {
        this.tenantName = tenantName;
        return this;
    }

    @Override
    public SDRestClient authenticate() throws AuthenticationException {
       return SDAuthenticator.invoke(new Credentials(user, password, tenantName), endpoint, config);
    }
}
