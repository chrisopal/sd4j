package com.hpe.china.sd.integration.client.api;


import com.hpe.china.sd.integration.core.api.IRestClientBuilder;

public interface ISDRestClientBuilder extends IRestClientBuilder<SDRestClient, ISDRestClientBuilder> {

    /**
     * Tenant Name Required for Rest Client Auth
     *
     * @param tenantName tenant name
     * @return SD client builder
     */
    ISDRestClientBuilder tenantName(String tenantName);

}
