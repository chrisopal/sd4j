package com.hpe.china.sd.integration.client.model.common;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PolicyParameter {

    /**
     * Parameter name
     */
    private String name;
    /**
     * Parameter value
     */
    private String value;
}
