package com.hpe.china.sd.integration.client.model.idenity;

import lombok.Data;

@Data
public class Token {

    /**
     * The id of the authentication token to be used to identify a session.
     * All subsequent requests must include an X-Auth-Token HTTP header containing this security token id.
     */
    private String id;

    /**
     * The time when this token will expire.
     * The value is provided as the number of milliseconds since the UNIX epoch
     */
    private String expires;

    /**
     * The time when this token will expire in a human-readable format.
     */
    private String expiresAsDate;

}
