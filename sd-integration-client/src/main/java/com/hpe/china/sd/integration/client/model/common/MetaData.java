package com.hpe.china.sd.integration.client.model.common;

import com.hpe.china.sd.integration.core.api.model.ModelEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.HashMap;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class MetaData extends HashMap<String, String> implements ModelEntity {

    private static final long serialVersionUID = 1L;
}
