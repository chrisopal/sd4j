package com.hpe.china.sd.integration.client.service.client;

import com.hpe.china.sd.integration.client.api.SDRestClient;
import com.hpe.china.sd.integration.client.constants.PathConstants;
import com.hpe.china.sd.integration.client.model.idenity.AuthResponse;
import com.hpe.china.sd.integration.client.model.idenity.Credentials;
import com.hpe.china.sd.integration.core.api.constants.ClientConstants;
import com.hpe.china.sd.integration.core.api.constants.ResponseCodes;
import com.hpe.china.sd.integration.core.api.exceptions.AuthenticationException;
import com.hpe.china.sd.integration.core.api.transport.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.hpe.china.sd.integration.core.api.transport.HttpExceptionHandler.mapException;

/**
 * @author gjxie
 *
 * Class Responsible for Authenticating and Re-Authenticating in case of token invalid or expire
 */
public class SDAuthenticator {


    private static final String TOKEN_INDICATOR = "Tokens";
    private static final Logger LOG = LoggerFactory.getLogger(SDAuthenticator.class);


    public static SDRestClient invoke(Credentials auth, String endpoint, Config config) {
        SessionInfo info = new SessionInfo(endpoint, false);
        return authenticate(auth, info, config);
    }

    /**
     * Re-authenticates/renews the token for the current Session
     */
    @SuppressWarnings("rawtypes")
    public static void reAuthenticate() {

        LOG.debug("Re-Authenticating session due to expired Token or invalid response");

        SDRestClientSession session = (SDRestClientSession)SDRestClientSession.getCurrent();

        SessionInfo info = new SessionInfo(session.getEndpoint(), true);
        authenticate(session.credentials, info, session.getConfig());

    }

    private static SDRestClient authenticate(Credentials auth,
                                             SessionInfo info, Config config) {
        HttpRequest<AuthResponse> request = HttpRequest.builder(AuthResponse.class)
                .header(ClientConstants.HEADER_SD4J_AUTH, TOKEN_INDICATOR).endpoint(info.endpoint)
                .method(HttpMethod.POST).path(PathConstants.TOKEN_PATH).config(config).entity(auth).build();

        HttpResponse response = HttpExecutor.create().execute(request);
        if (response.getStatus() >= 400) {
            try {
                throw mapException(response.getStatusMessage(), response.getStatus());
            } finally {
                HttpEntityHandler.closeQuietly(response);
            }
        }

        AuthResponse authResponse = response.getEntity(AuthResponse.class);
        String tokenId = null;
        if(authResponse != null && authResponse.getAccess() != null && authResponse.getAccess().getToken() != null){
            tokenId = authResponse.getAccess().getToken().getId();
            LOG.debug("Get Token Id: {}", tokenId);
            auth.setTokenId(tokenId);
        }
        if(StringUtils.isEmpty(tokenId)){
            throw new AuthenticationException("Empty token Id retrieved from server", ResponseCodes.UNAUTHORIZED);
        }

        if (!info.reLinkToExistingSession)
            return SDRestClientSession.createSession(info.endpoint, auth, config);

        SDRestClientSession current = (SDRestClientSession) SDRestClientSession.getCurrent();
        current.credentials = auth;
        return current;
    }

    private static class SessionInfo {
        String endpoint;
        boolean reLinkToExistingSession;

        SessionInfo(String endpoint, boolean reLinkToExistingSession) {
            this.endpoint = endpoint;
            this.reLinkToExistingSession = reLinkToExistingSession;
        }
    }
}
