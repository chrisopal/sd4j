package com.hpe.china.sd.integration.client.model.instance;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.hpe.china.sd.integration.client.model.catalog.CatalogRef;
import com.hpe.china.sd.integration.client.model.common.*;
import com.hpe.china.sd.integration.core.api.model.ModelEntity;
import lombok.Data;

@Data
public class ServiceResponse implements ModelEntity {

    private static final long serialVersionUID = 1L;

    /**
     * Service Version
     */
    private String version;

    /**
     * Result of the request. Defines whether or not the request could be successfully fulfilled.
     * <p>
     * result: string , x ∈ { SUCCESS , TRANSIENT_ERROR , RESOURCE_ERROR ,
     * REQUEST_ERROR , CONFIGURATION_ERROR , INTERNAL_ERROR }
     */
    private Result result;

    /**
     * The service id of the service
     */
    @JsonProperty("serviceid")
    private String serviceId;

    /**
     * Unique ID. The intent of UUIDs is to enable distributed systems to uniquely identify information without significant central coordination.
     */
    private String uuid;

    /**
     * The URI to use for later operations (e.g. delete or get) on this instance.
     */
    private String uri;

    /**
     * committed uri
     */
    @JsonProperty("committeduri")
    private String committedUri;

    /**
     * shadow uri
     */
    @JsonProperty("shadowuri")
    private String shadowUri;

    /**
     * Which kind of service. Same as in the request.
     */
    @JsonProperty("servicetype")
    private String serviceType;

    /**
     * The current state of the service. Is 'ACTIVE' when fully configured.
     */
    private State state;

    /**
     * Will be 'true' if the complete step for the service has been executed, 'false' otherwise.
     */
    private boolean completed;

    /**
     * The current rollback state of the service.
     * rollbackstate: x ∈ { NORMAL , CANCELED , ROLLBACK }
     */
    @JsonProperty("rollbackstate")
    private RollbackState rollbackState;

    /**
     * A map with all the parameter meta-data of the request or response.
     */
    @JsonProperty("metadata")
    private MetaData metadata;

    /**
     * The id of the transaction that carried out the last change on the service.
     */
    @JsonProperty("transactionid")
    private String transactionId;

    /**
     * The id of the order that carried out the last change on the service.
     */
    @JsonProperty("orderid")
    private String orderId;

    /**
     * The id of the tenant to which the service belongs.
     */
    @JsonProperty("tenantid")
    private String tenantId;

    /**
     * The name of the service.
     */
    @JsonProperty("servicename")
    private String serviceName;

    /**
     * The service's child type.
     */
    @JsonProperty("childtype")
    private ChildType childType;


    /**
     * Tells whether or not this is a shadow service.
     */
    @JsonProperty("is_shadow")
    private Boolean isShadow;

    /**
     *
     */
    @JsonProperty("is_task")
    private Boolean isTask;

    /**
     * Type and version of this instance.
     */
    @JsonProperty("servicetypeversion")
    private String serviceTypeVersion;

    /**
     * Service id of the parent of the service.
     */
    @JsonProperty("parentserviceid")
    private String parentServiceId;

    /**
     * Service id of instance referenced from this instance.
     */
    @JsonProperty("reference")
    private String reference;

    /**
     * Service id of instance referenced from this instance.
     */
    @JsonProperty("auxreference")
    private String auxReference;

    /**
     * Reference to the service catalog where this service type / version is defined.
     */
    @JsonProperty("catalog")
    private CatalogRef catalog;

    /**
     * Additional details about the parent of this service instance.
     */
    @JsonProperty("parentsvc")
    private ServiceRef parentSvc;

    /**
     * Additional details about the instance referenced from this service instance.
     */
    @JsonProperty("referencesvc")
    private ServiceRef referenceSvc;

    /**
     * Additional details about the instance referenced from this service instance.
     */
    @JsonProperty("auxreferencesvc")
    private ServiceRef auxReferenceSvc;

    /**
     * A message indicating what caused the error, if any.
     */
    @JsonProperty("errormessage")
    private String errorMessage;

    @JsonProperty("ignorederror")
    private boolean ignoredError;

    @JsonProperty("reason")
    private String reason;

    @JsonProperty("waitfor")
    private String waitFor;


}
