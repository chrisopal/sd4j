package com.hpe.china.sd.integration.client.api;

import com.hpe.china.sd.integration.client.api.domain.ServiceInstanceManagement;
import com.hpe.china.sd.integration.core.api.APIProvider;

import java.util.ServiceLoader;

/**
 * Provides access to the APIs and Buildables
 */
public class Apis {


    private static final APIProvider provider = initializeProvider();

    /**
     * Gets the API implementation based on Type
     *
     * @param <T> the API type
     * @param api the API implementation
     * @return the API implementation
     */
    public static <T> T get(Class<T> api) {
        return provider.get(api);
    }

    /**
     * @return service instance management service
     */
    public static ServiceInstanceManagement getInstanceManagementService() {

        return get(ServiceInstanceManagement.class);
    }


    /**
     * Initialize API Provider
     *
     * @return api providers in path
     */
    private static APIProvider initializeProvider() {
        // No need to check for emptiness as there is default implementation registered
        APIProvider p = ServiceLoader.load(APIProvider.class, Apis.class.getClassLoader()).iterator().next();
        p.initialize();
        return p;
    }
}
