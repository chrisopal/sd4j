package com.hpe.china.sd.integration.client.model;

import com.hpe.china.sd.integration.core.api.model.ModelEntity;

/**
 * Interface for Model Entity with Id and Msg properties like delete response
 */
public interface ModelIdEntity extends ModelEntity {

    /**
     * Get Id
     *
     * @return id
     */
    Long getId();

    /**
     * Get Operation Message
     *
     * @return operation message
     */
    String getMsg();
}
