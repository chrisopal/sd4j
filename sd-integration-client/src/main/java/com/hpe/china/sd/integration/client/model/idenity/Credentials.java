package com.hpe.china.sd.integration.client.model.idenity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.hpe.china.sd.integration.core.api.model.identity.AuthStore;

import java.io.Serializable;

@JsonRootName("auth")
public class Credentials extends Auth implements AuthStore {

    private static final long serialVersionUID = 1L;

    @JsonProperty
    private PasswordCredentials passwordCredentials = new PasswordCredentials();


    public Credentials() {
        super(Type.CREDENTIALS);
    }

    public Credentials(String username, String password) {
        this();
        passwordCredentials.setCredentials(username, password);
    }

    public Credentials(String username, String password, String tenantName) {
        this();
        passwordCredentials.setCredentials(username, password);
        setTenantName(tenantName);
    }


    @JsonIgnore
    public String getUsername() {
        return passwordCredentials.username;
    }

    @JsonIgnore
    public String getPassword() {
        return passwordCredentials.password;
    }

    @JsonIgnore
    @Override
    public String getId() {
        return null;
    }

    @JsonIgnore
    @Override
    public String getName() {
        return getTenantName();
    }

    private static final class PasswordCredentials implements Serializable {

        private static final long serialVersionUID = 1L;

        @JsonProperty
        String username;
        @JsonProperty
        String password;

        public void setCredentials(String username, String password) {
            this.username = username;
            this.password = password;
        }
    }
}
