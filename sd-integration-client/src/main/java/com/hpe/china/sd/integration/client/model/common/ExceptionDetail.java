package com.hpe.china.sd.integration.client.model.common;

import com.hpe.china.sd.integration.core.api.model.ModelEntity;
import lombok.Data;

@Data
public class ExceptionDetail implements ModelEntity {

    private static final long serialVersionUID = 1L;

    /**
     * The error code.
     */
    private Integer code;

    /**
     * A short message indicating the cause of this error.
     */
    private String message;

    /**
     * A detailed description of the cause of this error.
     */
    private String details;
}
