package com.hpe.china.sd.integration.client.model.instance;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.hpe.china.sd.integration.core.api.model.ModelEntity;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CreateServiceRequestWrapper implements ModelEntity {

    private static final long serialVersionUID = 1L;

    /**
     * Create Service Request Object
     */
    @JsonProperty("service")
    private CreateServiceRequest service;

}
