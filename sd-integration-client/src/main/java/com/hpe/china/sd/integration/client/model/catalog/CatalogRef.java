package com.hpe.china.sd.integration.client.model.catalog;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.hpe.china.sd.integration.client.model.common.Ref;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class CatalogRef extends Ref {

    /**
     * Package name of the referenced service specification in the catalog.
     */
    private String pkg;

    /**
     * Name of the referenced service specification in the catalog.
     */
    private String name;

    /**
     * Version of the referenced service specification in the catalog.
     */
    private String version;

    /**
     * Child type, service, resource, component
     */
    @JsonProperty("childtype")
    private String childType;

    /**
     * created uri
     */
    @JsonProperty("createuri")
    private String createUri;
}
