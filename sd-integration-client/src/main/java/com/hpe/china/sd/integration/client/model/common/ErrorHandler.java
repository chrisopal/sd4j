package com.hpe.china.sd.integration.client.model.common;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ErrorHandler {

    private PolicyType policy;
}
