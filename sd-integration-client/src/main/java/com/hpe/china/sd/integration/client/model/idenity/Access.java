package com.hpe.china.sd.integration.client.model.idenity;

import lombok.Data;

@Data
public class Access {

    private Token token;
}
