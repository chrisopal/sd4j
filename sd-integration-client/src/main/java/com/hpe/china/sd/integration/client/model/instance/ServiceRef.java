package com.hpe.china.sd.integration.client.model.instance;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.hpe.china.sd.integration.client.model.common.Ref;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;


@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ServiceRef extends Ref {

    /**
     * Service name
     */
    @JsonProperty("servicename")
    private String serviceName;

    /**
     * committedUri
     */
    @JsonProperty("committeduri")
    private String committedUri;

    /**
     * Shadow Uri
     */
    @JsonProperty("shadowuri")
    private String shadowUri;

    /**
     * Tells whether or not this is a shadow service.
     */
    @JsonProperty("is_shadow")
    private boolean isShadow;

}
