package com.hpe.china.sd.integration.client.model.instance;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.hpe.china.sd.integration.client.model.ObjectResult;
import com.hpe.china.sd.integration.client.model.common.ExceptionDetail;
import com.hpe.china.sd.integration.client.model.common.Result;
import com.hpe.china.sd.integration.core.api.model.ModelEntity;
import lombok.Data;

import java.util.List;

@Data
public class CreateServiceResponse implements ModelEntity {

    private static final long serialVersionUID = 1L;
    /**
     * The transaction id that was passed in the request.
     */
    @JsonProperty("transactionid")
    private String transactionId;

    /**
     * The unique identifier of the SOR message pertaining to this request;
     */
    @JsonProperty("processid")
    private String processId;

    /**
     * The order id that was passed in the request.
     */
    @JsonProperty("orderid")
    private String orderId;

    /**
     * The overall result of the operation.
     */
    @JsonProperty("result")
    private Result result;

    /**
     * Job Id
     */
    @JsonProperty("jobid")
    private Integer jobId;

    /**
     * Sor Id
     */
    @JsonProperty("sorid")
    private Integer sorId;

    /**
     * Only used for asynchronous callbacks
     */
    @JsonProperty("exception")
    private ExceptionDetail exception;

    /**
     * List of service responses containing detailed information about each service affected by the request.
     */
    @JsonProperty("serviceresponse")
    private List<ServiceResponse> serviceResponse;

    /**
     * In case of a roll back this field will contain the error that triggered the roll back process.
     */
    @JsonProperty("prerollbackexception")
    private ExceptionDetail preRollbackException;

    /**
     * List of service responses containing detailed information about what was the state of each service before the rollback process was triggered.
     */
    @JsonProperty("prerollbackserviceresponse")
    private List<ServiceResponse> preRollbackServiceResponse;


    @Data
    public static class CreateServiceResponseWrapper extends ObjectResult<CreateServiceResponse> {

        private CreateServiceResponse service;

        @Override
        protected CreateServiceResponse value() {
            return service;
        }
    }
}
