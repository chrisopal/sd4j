package com.hpe.china.sd.integration.client.model.instance;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.hpe.china.sd.integration.client.model.common.ErrorHandler;
import com.hpe.china.sd.integration.client.model.common.MetaData;
import com.hpe.china.sd.integration.core.api.model.ModelEntity;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CreateServiceRequest implements ModelEntity {

    private static final long serialVersionUID = 1L;

    @JsonProperty("transactionid")
    private String transactionId;

    @JsonProperty("processid")
    private String processId;

    @JsonProperty("orderid")
    private String orderId;

    @JsonProperty("metadata")
    private MetaData metadata;

    private ServiceRequest service;

    private ErrorHandler errorhandler;
}
