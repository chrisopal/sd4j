package com.hpe.china.sd.integration.client.constants;

public class PathConstants {

    //------------------------------------ Authentication -----------------------------------------------
    public static final String TOKEN_PATH = "/tokens";

    //------------------------------------ Service Instance Management ----------------------------------
    public static final String CREATE_SERVICE_PATH = "/%s/services";

}
