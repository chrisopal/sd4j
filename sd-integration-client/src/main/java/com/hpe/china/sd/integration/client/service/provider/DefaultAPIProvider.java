package com.hpe.china.sd.integration.client.service.provider;

import com.google.common.collect.Maps;
import com.hpe.china.sd.integration.client.api.domain.ServiceInstanceManagement;
import com.hpe.china.sd.integration.client.service.domain.ServiceInstanceManagementImpl;
import com.hpe.china.sd.integration.core.api.APIProvider;
import com.hpe.china.sd.integration.core.api.exceptions.ApiNotFoundException;

import java.util.Map;


/**
 *
 * Simple API Provider which keeps internally Maps interface implementations as singletons
 *
 */
public class DefaultAPIProvider implements APIProvider {

    private static final Map<Class<?>, Class<?>> bindings = Maps.newHashMap();
    private static final Map<Class<?>, Object> instances = Maps.newConcurrentMap();

    @Override
    public void initialize() {
        bind(ServiceInstanceManagement.class, ServiceInstanceManagementImpl.class);
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public <T> T get(Class<T> api) {
        if (instances.containsKey(api))
            return (T) instances.get(api);

        if (bindings.containsKey(api)) {
            try {
                T impl = (T) bindings.get(api).newInstance();
                instances.put(api, impl);
                return impl;
            } catch (Exception e) {
                throw new ApiNotFoundException("API Not found for: " + api.getName(), e);
            }
        }
        throw new ApiNotFoundException("API Not found for: " + api.getName());
    }

    private void bind(Class<?> api, Class<?> impl) {
        bindings.put(api, impl);
    }
}
