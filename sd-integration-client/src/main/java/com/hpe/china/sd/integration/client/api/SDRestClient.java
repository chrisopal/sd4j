package com.hpe.china.sd.integration.client.api;

import com.hpe.china.sd.integration.client.api.domain.ServiceInstanceManagement;
import com.hpe.china.sd.integration.core.api.AbstractRestClient;


/**
 *
 * Service Director Rest Client to expose all operations
 *
 */
public interface SDRestClient extends AbstractRestClient<SDRestClient> {
	
	/**
	 * Interface for service instance management
	 * 
	 * @return service instance management
	 */
	public ServiceInstanceManagement serviceInstanceManagement();

}
