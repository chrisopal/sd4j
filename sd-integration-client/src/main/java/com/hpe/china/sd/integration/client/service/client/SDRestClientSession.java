package com.hpe.china.sd.integration.client.service.client;

import com.hpe.china.sd.integration.client.api.Apis;
import com.hpe.china.sd.integration.client.api.SDRestClient;
import com.hpe.china.sd.integration.client.api.domain.ServiceInstanceManagement;
import com.hpe.china.sd.integration.client.model.idenity.Credentials;
import com.hpe.china.sd.integration.core.api.constants.ClientConstants;
import com.hpe.china.sd.integration.core.api.service.BaseClientSession;
import com.hpe.china.sd.integration.core.api.transport.Config;
import com.hpe.china.sd.integration.core.api.transport.HttpRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SDRestClientSession extends BaseClientSession<SDRestClientSession, SDRestClient> implements SDRestClient {

    private static final Logger log = LoggerFactory.getLogger(SDRestClientSession.class);

    Credentials credentials;
    private String endpoint;


    private SDRestClientSession(String endpoint, Credentials credentials, Config config) {
        this.endpoint = endpoint;
        this.credentials = credentials;
        this.config = config;
        sessions.set(this);
    }

    public static SDRestClientSession createSession(String endpoint, Credentials credentials,Config config) {
        SDRestClientSession session = new SDRestClientSession(endpoint, credentials, config);
        session.header(ClientConstants.HEADER_CONTENT_TYPE, ClientConstants.CONTENT_TYPE_JSON);
        return session;
    }

    @Override
    public String getEndpoint() {
        return endpoint;
    }

    @Override
    public String getTokenId() {
        return credentials != null ? credentials.getTokenId() : null;
    }

    @Override
    public <T> String getTokenId(HttpRequest<T> httpRequest) {

        return null;
    }

    @Override
    public String getAuthHeaderName() {
        return ClientConstants.HEADER_X_AUTH_TOKEN;
    }

    
    @Override
    public ServiceInstanceManagement serviceInstanceManagement() {
    	return Apis.getInstanceManagementService();
    }

}
