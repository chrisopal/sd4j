package com.hpe.china.sd.integration.client.model.idenity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.hpe.china.sd.integration.core.api.model.ModelEntity;
import lombok.Data;

@Data
public class AuthResponse implements ModelEntity {

    private static final long serialVersionUID = 1L;

    @JsonProperty
    private Access access = new Access();

}
