package com.hpe.china.sd.integration.client.model.idenity;

import com.fasterxml.jackson.annotation.JsonIgnore;

public abstract class Auth implements com.hpe.china.sd.integration.core.api.common.Auth {

    private static final long serialVersionUID = 1L;

    private String tenantName;

    @JsonIgnore
    private String tokenId;

    @JsonIgnore
    private transient Type type;

    protected Auth(Type type) {
        this.type = type;
    }

    public String getTenantName() {
        return tenantName;
    }

    public void setTenantName(String tenantName) {
        this.tenantName = tenantName;
    }

    @JsonIgnore
    public Type getType() {
        return type;
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }
}
