package com.hpe.china.sd.integration.client.model.instance;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.hpe.china.sd.integration.client.model.common.MetaData;
import com.hpe.china.sd.integration.client.model.common.RollbackState;
import com.hpe.china.sd.integration.client.model.common.State;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class DetailedServiceRef extends ServiceRef {

    /**
     * The id of the last order that created/managed this service.
     */
    @JsonProperty("orderid")
    private String orderId;

    /**
     * The order approval status (only valid for order responses).
     */
    @JsonProperty("orderapprovalstatus")
    private String orderApprovalStatus;

    /**
     * When the service was was created.
     */
    @JsonProperty("creationdate")
    private Long creationDate;

    /**
     * When the service was was created.
     */
    @JsonProperty("modificationdate")
    private Long modificationDate;

    /**
     * The name of this service as displayed on UI.
     */
    @JsonProperty("presentationname")
    private String presentationName;


    /**
     * The current state of this service.
     */
    private State state;

    /**
     * The desired state of this service.
     */
    @JsonProperty("desiredstate")
    private State desiredState;

    /**
     * The current rollback state of the service.
     * rollbackstate: x ∈ { NORMAL , CANCELED , ROLLBACK }
     */
    @JsonProperty("rollbackstate")
    private RollbackState rollbackState;

    /**
     * The display type of this service if set in the DSD; i.e. 'cfs', 'rfs', 'order', 'task', 'account'.
     */
    @JsonProperty("displaytype")
    private String displayType;

    /**
     * The specification package that this order belongs to.
     */
    @JsonProperty("specpkg")
    private String specPkg;

    /**
     * The specification name that this order is an instance of.
     */
    @JsonProperty("specname")
    private String specName;

    /**
     * Tells whether this service is a task. A service is considered to be a task if the descriptor set 'instance:' to 'TaskService'.
     */
    @JsonProperty("is_task")
    private boolean isTask;

    /**
     * The condition this service is waiting for (if any).
     */
    @JsonProperty("waitfor")
    private String waitFor;

    /**
     * A map of key-value pair strings representing all metadata for the service.
     */
    private MetaData metadata;

    /**
     * enabled if the service is protected.
     */
    @JsonProperty("protected__")
    private boolean isProtected;

    /**
     * A map with all the parameter meta-data of the request or response.
     */
    @JsonProperty("extraparameters")
    private MetaData extraParameters;
}
