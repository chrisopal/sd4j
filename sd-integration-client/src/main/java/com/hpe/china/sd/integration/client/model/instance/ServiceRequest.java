package com.hpe.china.sd.integration.client.model.instance;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.hpe.china.sd.integration.client.model.common.MetaData;
import com.hpe.china.sd.integration.core.api.model.ModelEntity;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ServiceRequest implements ModelEntity {

    private static final long serialVersionUID = 1L;

    /**
     * The action to perform. Supported values are 'create', 'modify' and 'delete'.
     */
    private String action;

    /**
     * Meta data parameters
     */
    private MetaData metadata;

    /**
     * d of the parent of the service. (Synonym for 'parentservicekey'.)
     */
    @JsonProperty("parentserviceid")
    private String parentServiceId;

    /**
     * Service id, name, uuid identifying the parent of the service.
     */
    @JsonProperty("parentservicekey")
    private String parentServiceKey;

    /**
     * Service id, name, uuid identifying the service.
     * This parameter is required unless provided in the URL or using the 'servicename' parameter.
     */
    @JsonProperty("servicekey")
    private String serviceKey;

    /**
     * Sets the service name of the new service. Currently, this parameter accept service name as well as service id or uuid.
     * However, support for service id or uuid may be removed in the future. (Synonym for 'servicekey'.)
     */
    @JsonProperty("servicename")
    private String serviceName;

    /**
     * Type of service entity (the name of a SP service descriptor). This parameter is required in service creation.
     */
    @JsonProperty("servicetype")
    private String serviceType;

    /**
     * The version of the SP service descriptor.
     */
    private String version;

    /**
     * Desired state of the service.
     */
    private String state;

    /**
     * See the documentation of the Common Network Resource Model (CRModel).
     */
    private String lifeCycleState;

}
