package com.hpe.china.sd.integration.client.model.common;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum Result {

    UNRECOGNIZED,
    SUCCESS,
    TRANSIENT_ERROR,
    RESOURCE_ERROR,
    REQUEST_ERROR,
    CONFIGURATION_ERROR,
    INTERNAL_ERROR;

    @JsonCreator
    public static Result value(String v) {
        if (v == null) return UNRECOGNIZED;
        try {
            return valueOf(v.toUpperCase());
        } catch (IllegalArgumentException e) {
            return UNRECOGNIZED;
        }
    }

    @JsonValue
    public String value() {
        return name().toUpperCase();
    }
}
