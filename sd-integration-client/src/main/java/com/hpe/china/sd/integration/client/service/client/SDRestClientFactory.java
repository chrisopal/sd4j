package com.hpe.china.sd.integration.client.service.client;

import com.hpe.china.sd.integration.client.api.ISDRestClientBuilder;
import com.hpe.china.sd.integration.client.api.SDRestClient;
import com.hpe.china.sd.integration.core.api.transport.Config;

/**
 * Factory Helper class to instantiate new SDRestClient
 */
public class SDRestClientFactory {

    //Default Endpoint
    private static final String DEFAULT_ENDPOINT = "http://localhost:8081/v2";

    //Default Tenant
    private static final String DEFAULT_AUTH_TENANT = "hpsa";

    /**
     * Get Client Builder to instantiate client
     *
     * @return Builder
     */
    public static ISDRestClientBuilder builder() {
        return new SDRestClientBuilder();
    }

    /**
     * Get New Client with username, password and default configuration
     *
     * @param username   username
     * @param password   password
     * @return client
     */
    public static SDRestClient newClient(String username, String password) {
        return newClient(username, password, DEFAULT_AUTH_TENANT, DEFAULT_ENDPOINT);
    }

    /**
     * Get New Client with merchantId & token
     *
     * @param username   username
     * @param password   password
     * @param authTenant authTenant
     * @return client
     */
    public static SDRestClient newClient(String username, String password, String authTenant) {
        return newClient(username, password, authTenant, DEFAULT_ENDPOINT);
    }

    /**
     * Get New Client with partnerId, partnerKey, shopId & endpoint, this should be
     * the most common way to instantiate a client
     *
     * @param username   username
     * @param password   password
     * @param authTenant authTenant
     * @param endpoint   endpoint
     * @return client
     */
    public static SDRestClient newClient(String username, String password, String authTenant, String endpoint) {
        return newClient(username, password, authTenant, endpoint, Config.DEFAULT);
    }

    /**
     * Get New Client with username, password, auth-tenant, endpoint and customized config
     *
     * @param username   username
     * @param password   password
     * @param authTenant authTenant
     * @param endpoint   endpoint
     * @param config     config
     * @return client
     */
    public static SDRestClient newClient(String username, String password, String authTenant, String endpoint, Config config) {

        return builder().tenantName(authTenant)
                .credentials(username, password)
                .endpoint(endpoint)
                .withConfig(config)
                .authenticate();
    }

}
