package com.hpe.china.sd.integration.client.model.common;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PolicyType {

    /**
     * Policy name.
     */
    private String name;

    /**
     * Parameters required for the error policy.
     */
    private PolicyParameters parameters;

    /**
     * Next error policy to apply in case this one fails
     */
    private PolicyType fallback;
}
