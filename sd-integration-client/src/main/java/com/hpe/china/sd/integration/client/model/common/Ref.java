package com.hpe.china.sd.integration.client.model.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.hpe.china.sd.integration.core.api.model.ModelEntity;
import lombok.Data;

@Data
public class Ref implements ModelEntity {

    private static final long serialVersionUID = 1L;
    /**
     * The id of the tenant to which the referenced item belongs
     */
    @JsonProperty("tenantid")
    private String tenantid;

    /**
     * The UUID in case of a service; the package/name/version in case of a catalog item
     */
    private String id;

    /**
     * The name of this reference (e.g. 'child', 'parent', 'reference', 'descriptor')
     */
    @JsonProperty("refname")
    private String refName;

    /**
     * The type of the referenced item (e.g. 'service', 'catalog', 'kind')
     */
    private String type;

    /**
     * The URI that can be used to access the referenced item
     */
    private String uri;

}
